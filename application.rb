# application.rb

LANG = 'en_GB.UTF-8'.freeze
ENV['RACK_ENV'] ||= 'development'
LOG_PATH = (ENV['RACK_ENV'] == 'production') ? "/var/log/gaptech-ops-api/logger.log" : "logger.log"


# base gems
require 'rubygems'
require 'bundler'
require 'logger'
require 'json'
require 'rest-client'
require 'resolv-replace'
require 'require_all'
require 'sanitize'
require 'optparse'

# sinatra gems
require 'sinatra/base'
require 'sinatra/contrib'
require 'sinatra/custom_logger'
require 'sinatra/activerecord'

Bundler.require :default, ENV['RACK_ENV'].to_sym

# logger setup
logger = Logger.new(LOG_PATH)
logger.level = Logger::INFO  
logger.datetime_format = '%a %d-%m-%Y %H%M'  
APP_LOG = logger
APP_LOG.info "RACK_ENV = #{ENV['RACK_ENV']}"

require_rel 'helpers'
require_relative './database'
require_rel 'models'
require_rel 'controllers'