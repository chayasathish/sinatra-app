# application_controller.rb
require './application'

class ApplicationController < Sinatra::Base

  configure do
    disable :protection
  end

  before do
    ActiveRecord::Base.connection.verify!
    # ActiveRecord::Base.verify_active_connections!
    content_type :json
    set_headers
  end

  def set_headers
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Expose-Headers'] = 'ETag'
    response.headers["Allow"] = "GET, POST, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "*,x-requested-with,Content-Type,If-Modified-Since,If-None-Match"
    response.headers['Access-Control-Max-Age'] = '86400'
    response.headers['X-Frame-Options'] = 'ALLOWALL'
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  options "*" do
    response.headers["Allow"] = "GET, POST, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
  end

  get '/' do
      failure_res = { "success" => "Hello Sinatra App" }
      json failure_res, status: 200
  end

end