# Load the DB configs
db_configs = YAML.load_file('./config/database.yml').freeze

# Pick the DB config based on rack environment
app_db_config = db_configs[ENV['RACK_ENV']]

# Connect to Postgres database
ActiveRecord::Base.establish_connection app_db_config